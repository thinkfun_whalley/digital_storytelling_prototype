importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

if (workbox) {
	console.log("Workbox loaded");

	// use network first strategies on javascript files
	workbox.routing.registerRoute(
		/\.js$/,
		new workbox.strategies.NetworkFirst()
	);
	
	// cache google fonts
	workbox.routing.registerRoute(
		/^https:\/\/fonts\.googleapis\.com/,
		new workbox.strategies.StaleWhileRevalidate({
			cacheName: 'google-fonts-stylesheets',
		}),
	);
	
	// cache css files, update in background
	workbox.routing.registerRoute(
		/\.css$/,
		new workbox.strategies.StaleWhileRevalidate({
			cacheName: 'css-cache',
		})
	);
	
	// cache HTML files, update in background
	workbox.routing.registerRoute(
		/\.html$/,
		new workbox.strategies.StaleWhileRevalidate({
			cacheName: 'html-cache',
		})
	);
	
	// cache image files for a week, update after a week
	workbox.routing.registerRoute(
		/\.(?:png|jpg|jpeg|svg|gif|ico)$/,
		// use cache if available
		new workbox.strategies.CacheFirst({
			cacheName: 'image-cache',
			plugins: [
				new workbox.expiration.Plugin({
					// cache a max of 60 images, should be fine for now
					maxEntries: 60,
					// cache for one week
					maxAgeSeconds: 60*60,
				})
			]
		})
	);


} else {
	console.log("Workbox failed to load.")
}

