// change visible tab
function changeTab(evt, tabName) {
var i, tabs, tabButtons;
tabs = document.getElementsByClassName("tab");
for (i = 0; i < tabs.length; i++) {
	tabs[i].style.display = "none";
}
tabButtons = document.getElementsByClassName("episoden-list-title");
for (i = 0; i < tabs.length; i++) {
	tabButtons[i].className = tabButtons[i].className.replace(" tab-selected", "");
}
document.getElementById(tabName).style.display = "block";
evt.currentTarget.className += " tab-selected";
}

// react to cookie or randomly pick either episoden or episoden-b
function goToReader() {
	if (!Cookies.get("contentType")) {
		if (Math.random() > 0.5) {
			Cookies.set("contentType", "episodical");
			window.location.href = "episodical.html"; 
		} else {
			Cookies.set("contentType", "novella");
			window.location.href = "novella.html"; 
		}
	} else {
		window.location.href = Cookies.get("contentType") + ".html";
	}
}

// go to the next chapter
function goToChapter(contentTypeBackup) {
	var currentChapterURL = "";
	// if there's no current chapter in cookies
	if (!Cookies.get("currentChapter")) {
		// if there IS a content type stored
		if (Cookies.get("contentType")) {
			// current chapter URL is contentType + 1
			currentChapterURL = Cookies.get("contentType") + "_1.html";
			// set current chapter to 1
			Cookies.set("currentChapter", "1");
			window.location.href = currentChapterURL;
		} else {
			// if there IS NO content type stored
			currentChapterURL = contentTypeBackup + "_1.html";
			window.location.href = currentChapterURL;
		}
	} else {
		// if there IS a current chapter in cookies 
		currentChapterURL = Cookies.get("contentType") + "_" + Cookies.get("currentChapter") + ".html";
		window.location.href = currentChapterURL;
	}
}

// set the chapter for the episode select button
function setChapter(number, title) {
	Cookies.set("currentChapter", number);
	Cookies.set("currentChapterTitle", title);
}


// back button 
function goBack() {
	window.history.back();
}
// back to home
function goBackToHome() {
	window.location.href = "home.html";
}
// back to novella button 
function goBackToNovella() {
	window.location.href = "novella.html";
}
// back to episodical button 
function goBackToEpisodical() {
	window.location.href = "episodical.html";
}

// go to survey
const sub_choice = "premium";
function subscriptionEpisodical() {
	window.location.href = "survey-e.html#" + sub_choice;
}

function subscriptionNovella() {
	window.location.href = "survey-n.html#" + sub_choice;
}

// go to individual subscriptions
function subscriptionFromEpisodicalReader(from_page) {
	window.location.href = "subscription-e.html#" + from_page;
}

function subscriptionFromNovellaReader(from_page) {
	window.location.href = "subscription-n.html#" + from_page;
}

function lockedContentClick() {
	$(".lock-content-popup").removeClass("hidden-popup");
	$("body").addClass("noscroll");
}

function subscriptionFromEpisodicalLock() {
	window.location.href = "subscription-e.html#episodical-home";
	$("body").addClass("noscroll");
}

// Reader font buttons 
function changeFontSize(value) {
	Cookies.set("fontSize", value);
	$(".reader").removeClass("first-size");
	$(".reader").removeClass("second-size");
	$(".reader").removeClass("third-size");
	$(".reader").addClass(value);
	$(".font-size-button").removeClass("active");
	$("#" + value).addClass("active");
}
function changeReaderBackground(value) {
	Cookies.set("documentColor", value);
	$(".reader").removeClass("first-color");
	$(".reader").removeClass("second-color");
	$(".reader").removeClass("third-color");
	$(".reader").addClass(value);
	$(".font-color-button").removeClass("active");
	$("#" + value).addClass("active");
}
function changeFontFamily(value) {
	Cookies.set("fontFace", value)
	$(".reader").removeClass("first-face");
	$(".reader").removeClass("second-face");
	$(".reader").removeClass("third-face");
	$(".reader").removeClass("fourth-face");
	$(".reader").addClass(value);
	$(".font-face-button").removeClass("active");
	$("#" + value).addClass("active");
}

function shareOnTwitter() {
	var url = window.location.href;
	var text = "I love this story!";
	window.open('http://twitter.com/share?url='+encodeURIComponent(url)+'&text='+encodeURIComponent(text), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
	$(".share-popup").addClass("hidden-popup");
	$("body").removeClass("noscroll");
}

function shareOnWhatsapp() {
	var url = window.location.href;
	window.location = "whatsapp://send?text=I love this story! " + url;
	$(".share-popup").addClass("hidden-popup");
	$("body").removeClass("noscroll");
}

function shareOnEmail() {
	var url = window.location.href;
	window.location = "mailto:?subject=Check out SerialStorys!&body=I love this story! " + url;
	$(".share-popup").addClass("hidden-popup");
	$("body").removeClass("noscroll");
}

var liked = false;
function toggleLikes() {
	if (!liked) {
		$("#likes").html("392");
		document.getElementById('likes-icon').src = "img/icon-likes-filled.png";
		document.getElementById('likes-button-image').src = "img/like-icon-liked.png";
		$(".like").addClass("liked");
		liked = true;
	} else {
		$("#likes").html("391");
		document.getElementById('likes-icon').src = "img/icon-likes.png";
		document.getElementById('likes-button-image').src = "img/like-icon.png";
		$(".like").removeClass("liked");
		liked = false;
	}
}

// Install functionality
var deferredPrompt;
window.addEventListener('beforeinstallprompt', function(e) {
	e.preventDefault();
	// Stash the event so it can be triggered later.
	deferredPrompt = e;
	$(".install-app").removeClass("hidden-install");
});

function installAppToHomescreen() {
	deferredPrompt.prompt();
	$(".install-app").addClass("hidden-install");
	setTimeout(function() {
		$(".install-app").remove();
	}, 250);
	deferredPrompt.userChoice
		.then(function(choiceResult) {
			if (choiceResult.outcome === 'accepted') {
				console.log('User accepted install app to homescreen');
			} else {
				console.log('User dismissed the install message');
			}
			deferredPrompt = null;
		})
}
if (window.matchMedia('(display-mode: standalone)').matches) {
	$(".install-app").remove();
}

// reset scroll for reader pages
function resetScrollForPage(pageName) {
	localStorage.setItem("scrollPosition_/" + pageName, "0");
}

// register carousel function for popups 
(function($){
	$.fn.carousel = function(options) {
		var settings = {
			slider: ".bonus-wrapper",
			slide: ".bonus-page",
			previous: ".back-bonus-page",
			next: ".forward-bonus-page",
			pages: ".page"
		}

		options = $.extend(settings, options);

		return this.each(function() {
			var $element = $(this),
			$slider = $element.find(options.slider),
			$slide = $slider.find(options.slide),
			$previous = $element.find(options.previous),
			$next = $element.find(options.next),
			$pages = $element.find(options.pages),
			currentPage = 0,
			numberofPages = $pages.length;
	

			$element.find("> section").css("overflow", "hidden");
			
			$pages.eq(0).addClass("selected-page");

			$previous.addClass("invisible-pagination");

			var showSlide = function(direction) {
				var singleWidth = $slide.eq(0).outerWidth(),
				left = (direction == 1) ? "+=" + singleWidth : "-=" + singleWidth;

				$slider.stop(true, true).animate({
					scrollLeft: left
				}, 0);

				if (currentPage < numberofPages && currentPage >= 0) {
					if (currentPage < numberofPages) {
						currentPage += direction;
					}
					if (currentPage == numberofPages) {
						currentPage = numberofPages - 1;
					}
					if (currentPage < 0) {
						currentPage = 0;
					}
					$pages.removeClass("selected-page");
					$pages.eq(currentPage).addClass("selected-page");
				}
				if (currentPage == 0) {
					$previous.addClass("invisible-pagination");
				} else {
					$previous.removeClass("invisible-pagination")
				}
				if (currentPage == (numberofPages - 1)) {
					$next.addClass("invisible-pagination");
				} else {
					$next.removeClass("invisible-pagination")
				}
			}

			$previous.click(function(e) {
				e.preventDefault();
				showSlide(-1);
			})

			$next.click(function(e) {
				e.preventDefault();
				showSlide(1);

			})

		})
	}
})(jQuery);


$(function() {

	$("body").removeClass("body-load");

	// episode select button controller/content updater
	if ($(".episoden-select-button").length > 0) {
		nextEpisodeSelectButton();
	}
	function nextEpisodeSelectButton() {
		if (Cookies.get("currentChapter")) {
			$("#episode-title-header").html("Episode " + Cookies.get("currentChapter") + " jetzt lesen!");
			$("#episode-title-name").html(Cookies.get("currentChapterTitle"));
		} else {
			$("#episode-title-header").html("Episode 1 jetzt lesen!");
			$("#episode-title-name").html("Pilot");
		}
	}

	// Footer links
	$("#datenschutz-footer").click(function(e){
		e.preventDefault();
		$("#datenschutz").css("margin-left", "0vw");
	})
	$("#impressum-footer").click(function(e){
		e.preventDefault();
		$("#impressum").css("margin-left", "0vw");
	})
	$(".sidebar-nav-button").click(function(e){
		e.preventDefault();
		$("#datenschutz").css("margin-left", "100vw");
		$("#impressum").css("margin-left", "100vw");
	})

	// Reader menu popouts
	$("#episodelist").click(function(e){
		e.preventDefault();
		$("#chapter-menu").css("margin-left", "20vw");
		$("#chapter-menu").addClass("chapter-expanded");
		$("body").addClass("noscroll");
	})

	$(document).mouseup(function(e){
		if ($("#chapter-menu").hasClass('chapter-expanded')) {
			if (!$(e.target).parents('.greyed-out').length > 0) {
				$("#chapter-menu").css("margin-left", "110vw");
				$("#chapter-menu").removeClass("chapter-expanded");
				$("body").removeClass("noscroll");
				}
		}
	});

	// Reader specific controls
	if ($(".reader").length > 0) {
		// Set and keep font changes
		if (Cookies.get("fontSize")) {
			$(".reader").removeClass("first-size");
			$(".reader").removeClass("second-size");
			$(".reader").removeClass("third-size");
			$(".reader").addClass(Cookies.get("fontSize"));
			$(".font-size-button").removeClass("active");
			$("#" + Cookies.get("fontSize")).addClass("active");
		} else {
			$(".reader").addClass("second-size");
		}
		if (Cookies.get("documentColor")) {
			$(".reader").removeClass("first-color");
			$(".reader").removeClass("second-color");
			$(".reader").removeClass("third-color");
			$(".reader").addClass(Cookies.get("documentColor"));
			$(".font-color-button").removeClass("active");
			$("#" + Cookies.get("documentColor")).addClass("active");
		} else {
			$(".reader").addClass("first-color");
		}
		if (Cookies.get("fontFace")) {
			$(".reader").removeClass("first-face");
			$(".reader").removeClass("second-face");
			$(".reader").removeClass("third-face");
			$(".reader").removeClass("fourth-face");
			$(".reader").addClass(Cookies.get("fontFace"));
			$(".font-face-button").removeClass("active");
			$("#" + Cookies.get("fontFace")).addClass("active");
		} else {
			$(".reader").addClass("third-face");
		}
		
		// Reader font size changer options
		var fontOpen = false;
		$("#fontsize").click(function(){
			if (fontOpen == false) {
				$(".font-pane").removeClass("hidden");
				$("body").addClass("noscroll");
				fontOpen = true;
			} else {
				$(".font-pane").addClass("hidden");
				$("body").removeClass("noscroll");
				fontOpen = false;
			}
		})
		$(".reader").click(function() {
			if (fontOpen == true) {
				$(".font-pane").addClass("hidden");
				$("body").removeClass("noscroll");
				fontOpen = false;
			}
		})
		
		// Hide header on scroll
		var didScroll;
		var lastScrollPosition = 0;
		var delta = 5;
		var navbarHeight = $('.reader-header').outerHeight();

		// Progress bar
		var winHeight = $(window).height(), 
			docHeight = $(document).height(),
			progressBar = $("progress"),
			maxScrollable, scrollValue;
		
		// Set the max scrollable area for progress bar
		maxScrollable = docHeight - winHeight;
		progressBar.attr('max', maxScrollable);
		
		// var needed for saving reading position
		var currentPageName = document.location.pathname;

		$("body").scroll(function(event){
			didScroll = true;
			scrollValue = $("body").scrollTop();
			progressBar.attr('value', scrollValue);
			localStorage.setItem("scrollPosition_" + currentPageName, scrollValue.toString());
		});

		setInterval(function() {
			if (didScroll) {
				hasScrolled();
				didScroll = false;
			}
		}, 250);

		function hasScrolled() {
			var scrollTop = $("body").scrollTop();
			
			// Make sure they scroll more than delta
			if(Math.abs(lastScrollPosition - scrollTop) <= delta)
				return;
			
			// If they scrolled down and are past the navbar, add class .nav-up.
			// This is necessary so you never see what is "behind" the navbar.
			if (scrollTop > lastScrollPosition && scrollTop > navbarHeight){
				setTimeout(function() {$(".reader-header").removeClass("scroll-down").addClass("scroll-up")}, 100);
			} else {
				if(scrollTop + $(window).height() < $(document).height()) {
					setTimeout(function() {$(".reader-header").removeClass("scroll-up").addClass("scroll-down")}, 100);
				}
			}
			
			lastScrollPosition = scrollTop;
		}
		
		// if this reader page has a scroll position in local storage, go to it on load
		if (localStorage["scrollPosition_" + currentPageName]) {
			$("body").scrollTop(localStorage.getItem("scrollPosition_" + currentPageName));
		}

		// Attempt to change link if scroll at bottom of page
		var clientY;
		$("a.next-episode").on("touchstart", function(e) {
			clientY = e.originalEvent.touches[0].clientY;
			return false;
		})
		$("a.next-episode").on("touchend", function(e) {
			var newY = e.originalEvent.changedTouches[0].clientY;
			if (newY <= clientY) {
				resetScrollForPage($(this).attr("href"));
				setChapter($(this).data("chapter-number"), $(this).data("chapter-title"));
				window.location.href = $(this).attr("href");
			}
			return false;
		})



	}

	// Subscription controls
	$(".basis").click(function(){
		$(".basis").addClass("selected");
		$(".standard").removeClass("selected");
		$(".premium").removeClass("selected");
		$(".subscription-button").prop("id", "selected-basis");
		sub_choice = "basis";
	})
	$(".standard").click(function(){
		$(".basis").removeClass("selected");
		$(".standard").addClass("selected");
		$(".premium").removeClass("selected");
		$(".subscription-button").prop("id", "selected-standard");
		sub_choice = "standard";
	})
	$(".premium").click(function(){
		$(".basis").removeClass("selected");
		$(".standard").removeClass("selected");
		$(".premium").addClass("selected");
		$(".subscription-button").prop("id", "selected-premium");
		sub_choice = "premium";
	})


	// Survey form disable/enable weiter button based on user input 
	$("#survey-question-one").keyup(function () {
		if($(this).val().length > 2) {
			$("#survey-first-button").removeClass("survey-hidden");
		} else {
			$("#survey-first-button").addClass("survey-hidden");
		}
	});
	$("#survey-question-two").keyup(function () {
		if($(this).val().length > 2) {
			$("#survey-second-button").removeClass("survey-hidden");
		} else {
			$("#survey-second-button").addClass("survey-hidden");
		}
	});
	$("#survey-question-three").keyup(function () {
		if($(this).val().length > 2) {
			$("#survey-submit").removeClass("survey-hidden");
			$("#survey-submit").removeAttr("disabled");
		} else {
			$("#survey-submit").addClass("survey-hidden");
            $("#survey-submit").attr('disabled', 'disabled');
		}
	});

	// Survey form controls
	$("#survey-first-button").click(function(){
		if ($.trim($("#survey-question-one").val())) {
			$("#first-survey-field").addClass("offscreen-right");
			$("#second-survey-field").removeClass("offscreen-left");
		} else { 
			$("#survey-question-one").css("border", "3px solid red"); 
		}
	})
	$("#survey-second-button").click(function(){
		if ($.trim($("#survey-question-two").val())) {
			$("#second-survey-field").addClass("offscreen-right");
			$("#third-survey-field").removeClass("offscreen-left");
		}
	})

	$(".survey-input").oninput = function(){
		$(this).css("border", "1px solid #ff358a;")
	};


	// Unavailable message appear/disappear
	$(".cover-image").click(function(){
		$(".cover-image img").addClass("faded");
		$(".unavailable").removeClass("hidden-popup");
		$("body").addClass("noscroll");
	})

	if ($(".cover-image").length > 0) {
		$(document).mouseup(function(e){
			if ($(e.target).hasClass('.unavailable')) return false;
			$(".faded").removeClass("faded");
			$(".unavailable").addClass("hidden-popup");
			$("body").removeClass("noscroll");
		});
	}

	// Locked content popup controls 
	if ($(".lock-content-popup").length > 0) {
		$(document).mouseup(function(e){
			if ($(e.target).hasClass('.lock-content-button')) return false;
			$(".lock-content-popup").addClass("hidden-popup");
			$("body").removeClass("noscroll");
		});
	}

	// bonus material scroll handles
	if ($(".bonus-tabs").length > 0) {
		$(".bonus-content").carousel();
	}

	// Chapter select toggle hider
	const toggleCollapse = document.getElementsByClassName("expand");

	for (var i = 0; i < toggleCollapse.length; i++) {
		toggleCollapse[i].addEventListener("click", function() {
			var content = this.nextElementSibling;
			var icon = $(this).children(".expand-icon");
			if (content.style.maxHeight){
			  content.style.maxHeight = null;
			  icon.html("<span>+</span>");
			} else {
			  content.style.maxHeight = content.scrollHeight + "px";
			  icon.html("<span>-</span>");
			} 
		});
	}

	// Popup var to control story universe having many popups
	var isPopup = false;
	// Popup pane toggle hider
	$(".reader-popup-display").click(function() {
		if (!isPopup) {
			var content = this.nextElementSibling;
			content.classList.remove("hidden-popup");
			if ($(".universum-more").length > 0) {
				isPopup = true;
			}
			$("body").addClass("noscroll");
		} else {
			$(".universum-more").addClass("hidden-popup");
			isPopup = false;
		}
	});

	// Story Universe submenu toggle hider
	$(".submenu-display").click(function(e) {
		$(this).parent().parent().parent().parent().addClass("hidden-popup");
		$("#" + ($(this).attr('data-attr'))).removeClass("hidden-popup");
		$("body").addClass("noscroll");
	});
	
	// close element button
	$(".close-element").click(function(e){
		e.target.parentElement.classList.add("hidden-popup");
		isPopup = false;
		$("body").removeClass("noscroll");
	})
	
	// close bonus button
	$(".close-bonus").click(function(e){
		e.target.parentElement.classList.add("hidden-popup");
		$("#" + ($(this).attr('data-attr'))).removeClass("hidden-popup");
	})


	// install app
	var appInstalled;
	if ($(".install-app")) {
	//	$(".install-app").removeClass("hidden-install");

		$("header, .install-app-close, .home-landing, .home").click(function() {
			$(".install-app").addClass("hidden-install");
			setTimeout(function() {
				$(".install-app").remove();
			}, 250);
		})
	}

	// scroll to top of datenschutz
	$(".scrollTop").click(function() {
		$("#datenschutz").animate({ scrollTop: 0 }, "slow");
		return false;
	});
	$(".scrollTopStandalone").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	})

});



// register service worker code. Ignores unsupported browsers
if ('serviceWorker' in navigator) {
	window.addEventListener('load', function() {
		navigator.serviceWorker.register('service-worker.js').then(function() {
			console.log('[ServiceWorker] Client: service worker registration complete.');
		}, function() {
			console.log('[ServiceWorker] Client: service worker registration failure.');
		});
	})
} else {
	console.log('[ServiceWorker] Client: service worker is not supported.');
}