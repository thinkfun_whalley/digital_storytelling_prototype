var express = require("express");
var router = express.Router();

const mailjetKeys = require('./mailjet_api.json');
const mailjet = require('node-mailjet').connect(mailjetKeys.APIKey, mailjetKeys.secretKey);
var app = express();

app.use(express.static('public'));
app.use(express.urlencoded());

app.use('/covers', express.static(__dirname + '/public/covers'));
app.use('/books', express.static(__dirname + '/public/books'));
app.use('/images', express.static(__dirname + '/public/img'));
app.use('/reader', express.static(__dirname + '/public/reader'));



app.post('/survey-response.html', function(req, res) {
	let first_answer = req.body.question_one;
	let second_answer = req.body.question_two;
	let third_answer = req.body.question_three;
	const request = mailjet
	.post("send", {'version': 'v3.1'})
	.request({
		"Messages":[
				{
						"From": {
								"Email": "noreply@ravensburger.com",
								"Name": "SerialStorys Survey Feedback"
						},
						"To": [
								{
										"Email": "heike.bodenmueller@ravensburger.de",
										"Name": "Bodenmueller, Heike"
								}
						],
						"Subject": "SerialStorys Survey Response",
						"TextPart": "<p>SerialStorys reader survey response:</p><p>" + first_answer + "</p><p>" + second_answer + "</p><p>" + third_answer + "</p>",
						"HTMLPart": "<p>SerialStorys reader survey response:</p><p>" + first_answer + "</p><p>" + second_answer + "</p><p>" + third_answer + "</p>"
				}
		]
	});

    request
        .then((result) => {
            res.sendFile(__dirname + "/public/survey-response.html");
        })
        .catch((err) => {
            console.log(err.statusCode);
        });
});

var server = app.listen(8837, function(){
    var port = server.address().port;
    console.log("Server started at http://localhost:%s", port);
});